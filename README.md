# Como compilar:


#### Para compilar o EPWiiP voce irá precisar do [DevkitPro](https://devkitpro.org/wiki/Main_Page) e das bibiotecas disponiveis em wii-dev

#### Apos ter o devkitpro instalado, voce pode rodar o Makefile escrevendo "make" no chat

## Atenção!

#### é possivel que voce encontre um "export DEVKITPPC=/opt/devkitpro/devkitPPC" no inicio do Makefile, ou seja, caso apareça algum erro relacionado ao DEVKITPPC, comente essa linha com um "\#" e tente executar de novo o Makefile

--------------------------

# Como baixar:

#### Vá na parte de [releases](https://gitlab.com/LucasPB710/epwiip/-/releases) e baixe a mais recente, pegue a pasta "EdnaldoPereiraPlayer" e coloque na pasta apps do seu cartão SD ou do seu Pendrive

#### Para abrir, basta utilizar o homebrew channel
